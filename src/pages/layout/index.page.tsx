import type { NextPage } from 'next';
import Head from 'next/head';
import { clone } from 'ramda';
import { SourceAccordionList } from '../../components/SelectSource/SourceAccordionList';
import { useTeleContext } from '../../context/TeleContext';
import { useSavedLayout } from '../../hooks/useSavedLayout';
import { MainLayout } from '../../layout/MainLayout';
import { Source } from '../../sources';
import { Layout } from '../../components/Layout/Layout';
import { ColType, RowType, SourceNode } from './types';

const LayoutPage: NextPage = () => {
  const { isEditing, editingSourceUuid } = useTeleContext();
  const [layout, setLayout] = useSavedLayout();

  const handleSourceChange = (source: Source) => {
    if (!editingSourceUuid) return;
    handleSelectSource({ sourceSlug: source.slug, uuid: editingSourceUuid });
  };

  const handleSelectSource = (node: SourceNode) => {
    setLayout(l => {
      const layoutClone = clone(l);
      if (!layoutClone) return layoutClone;

      const findInRow = (row: RowType) => {
        if (row.cols) {
          for (const col of row.cols) {
            findInCol(col);
          }
        }
      };
      const findInCol = (col: ColType) => {
        if (col.node?.uuid === node.uuid) {
          col.node = node;
          return;
        }
        if (col.rows) {
          for (const row of col.rows) {
            findInRow(row);
          }
        }
      };
      findInCol(layoutClone);
      return layoutClone;
    });
  };

  return (
    <MainLayout>
      <Head>
        <title>Tele - Layout</title>
        <meta name="description" content="Generated by create next app" />
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <div className="row">
        <div className={isEditing ? 'col-8' : 'col-12'}>
          <div className="row no-gutters row-canales">
            {layout && (
              <Layout layout={layout} onSourceChange={handleSelectSource} />
            )}
          </div>
        </div>
        {isEditing && (
          <div className="col-4 pr-4">
            <SourceAccordionList
              onSelect={handleSourceChange}
              selectedSourceSlug={editingSourceUuid}
            />
          </div>
        )}
      </div>
    </MainLayout>
  );
};

export default LayoutPage;
